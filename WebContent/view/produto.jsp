<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@page import="java.util.List"%>
<%@page import="java.util.Arrays"%>

<%@page import="model.Produto"%>
<%@page import="model.Fornecedor"%>

<%@page import="dao.ProdutoDao"%>
<%@page import="dao.FornecedorDao"%>

<!DOCTYPE html>
<html>
<head>
<title>Produtos</title>
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no" />
<link rel="stylesheet" type="text/css" href="../css/main.css">
<link
	href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css"
	rel="stylesheet" crossorigin="anonymous" />
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/js/all.min.js"
	crossorigin="anonymous"></script>

</head>

<!-- Referencia Template: https://startbootstrap.com/templates/sb-admin/  -->

<body class="sb-nav-fixed">
	<nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
		<a class="navbar-brand" href="/super/index.jsp">Supermarcado R&R</a>
		<button class="btn btn-link btn-sm order-1 order-lg-0"
			id="sidebarToggle" href="#">
			<i class="fas fa-bars"></i>
		</button>


	</nav>

	<div id="layoutSidenav">
		<div id="layoutSidenav_nav">
			<nav class="sb-sidenav accordion sb-sidenav-dark"
				id="sidenavAccordion">
				<div class="sb-sidenav-menu">
					<div class="nav">

						<div class="sb-sidenav-menu-heading">Operador</div>

						<a class="nav-link" href="/super/view/produto">
							<div class="sb-nav-link-icon">
								<i class="fas fa-gifts"></i>
							</div> Produtos

						</a> <a class="nav-link" href="/super/view/fornecedor">
							<div class="sb-nav-link-icon">
								<i class="fas fa-user-tag"></i>
							</div> Fornecedores

						</a>
					</div>
				</div>

			</nav>
		</div>

		<div id="layoutSidenav_content">

			<form id="formFornecedor" method="get">
				<main
					style="margin-left: 10px; margin-right: 10px; margin-top: 30px; margin-bottom: 30px;">
					<div class="container-fluid">
						<div class="form-row">
							<div class="col-md-3 mb-3">
								<label for="validationDefault01">Código</label> <input
									type="text" class="form-control" name="codProduto" readonly
									value="${produtoParam.getId()}">
							</div>
							<div class="col-md-4 mb-3">
								<label for="validationDefault03">Código de Barras</label> <input
									type="text" class="form-control" name="codBarras"
									value="${produtoParam.getCodBarras()}">
							</div>
						</div>

						<div class="form-row">
							<div class="col-md-12 mb-3">
								<label for="validationDefault01">Nome</label> <input type="text"
									class="form-control" name="nomeProduto"
									value="${produtoParam.getNome()}">
							</div>
						</div>

						<div class="form-row">
							<div class="col-md-6 mb-3">
								<label for="validationDefault04">Fornecedor</label> <select
									class="custom-select" name="fornecedorProduto">
									<option selected>${produtoParam.getFornecedor()}</option>

									<%
										FornecedorDao fornDao = new FornecedorDao();
									List<Fornecedor> lstProd = fornDao.getAll();

									for (Fornecedor fornProd : lstProd) {
									%>

									<option><%=fornProd.getRazaoSocial()%></option>

									<%
										}
									%>

								</select>
							</div>
							<div class="col-md-6 mb-3">
								<label for="validationDefault04">Origem</label> <select
									class="custom-select" name="origemProduto">
									<option selected>${produtoParam.getOrigemProduto()}</option>
									<option>Importado</option>
									<option>Nacional</option>
								</select>
							</div>
						</div>

						<div class="form-row">
							<div class="col-md-4 mb-3">
								<label>Preço custo</label>
								<div class="input-group mb-3">
									<div class="input-group-prepend">
										<span class="input-group-text">R$</span>
									</div>
									<input type="number" step="any" class="form-control"
										name="precoCusto" value="${produtoParam.getPrecoCusto()}">
								</div>
							</div>

							<div class="col-md-4 mb-3">
								<label>Lucro</label>
								<div class="input-group mb-3">
									<div class="input-group-prepend">
										<span class="input-group-text">%</span>
									</div>
									<input type="number" step="any" max="150" class="form-control"
										name="percLucro" value="${produtoParam.getPercLucro()}">
								</div>
							</div>

							<div class="col-md-4 mb-3">
								<label>Preço de Venda</label>
								<div class="input-group mb-3">
									<div class="input-group-prepend">
										<span class="input-group-text">R$</span>
									</div>
									<input type="number" step="any" readonly class="form-control"
										name="precoVenda" value="${produtoParam.getPrecoVenda()}">
								</div>
							</div>
						</div>

						<button class="btn btn-primary" type="submit" name="action"
							value="save">Salvar</button>
					</div>
				</main>
			</form>

			<div class="card mb-4" style="margin-left: 20px; margin-right: 20px;">
				<div class="card-header">
					<i class="fas fa-table mr-1"></i>Produtos
				</div>

				<div class="card-body">
					<div>
						<table class="table table-bordered" width="100%" cellspacing="0">
							<thead>
								<th>Código</th>
								<th>Nome</th>
								<th>Barras</th>
								<th>R$ Custo</th>
								<th>R$ Venda</th>
								<th>Editar</th>
								<th>Excluir</th>
							</thead>
							<tbody>

								<%
									ProdutoDao dao = new ProdutoDao();
								List<Produto> lista = dao.getAll();

								for (Produto f : lista) {
								%>

								<tr>
									<th id="id" scope="row"><%=f.getId()%></th>
									<td><%=f.getId()%></td>
									<td><%=f.getNome()%></td>
									<td><%=f.getCodBarras()%></td>
									<td><%=f.getPrecoCusto()%></td>

									<td align="center"><a
										onclick="this.href='/super/view/produto?action=alt&codProduto='+<%=f.getId()%>">
											<i class="fas fa-user fa-edit"></i>
									</a></td>

									<td align="center"><a name="excluirForn"
										onclick="this.href='/super/view/produto?action=del&codProduto='+<%=f.getId()%>">
											<i class="fas fa-user fa-trash-alt"></i>
									</a></td>

								</tr>
								<%
									}
								%>

							</tbody>
						</table>
					</div>
				</div>
			</div>

		</div>

	</div>
	<script src="https://code.jquery.com/jquery-3.5.1.min.js"
		crossorigin="anonymous"></script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.bundle.min.js"
		crossorigin="anonymous"></script>
	<script src="../js/scripts.js"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js"
		crossorigin="anonymous"></script>
	<script src="../js/chart-area-demo.js"></script>
	<script src="../js/chart-bar-demo.js"></script>
	<script
		src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"
		crossorigin="anonymous"></script>
	<script
		src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"
		crossorigin="anonymous"></script>
	<script src="../js/datatables-demo.js"></script>
</body>
</html>