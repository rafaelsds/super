<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@page import="java.util.List"%>
<%@page import="java.util.Arrays"%>

<%@page import="model.Fornecedor"%>
<%@page import="dao.FornecedorDao"%>

<!DOCTYPE html>
<html>
<head>
<title>Fornecedores</title>
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no" />
<link rel="stylesheet" type="text/css" href="../css/main.css">
<link
	href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css"
	rel="stylesheet" crossorigin="anonymous" />
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/js/all.min.js"
	crossorigin="anonymous"></script>

</head>

<!-- Referencia Template: https://startbootstrap.com/templates/sb-admin/  -->

<body class="sb-nav-fixed">
	<nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
		<a class="navbar-brand" href="/super/index.jsp">Supermarcado R&R</a>
		<button class="btn btn-link btn-sm order-1 order-lg-0"
			id="sidebarToggle" href="#">
			<i class="fas fa-bars"></i>
		</button>


	</nav>

	<div id="layoutSidenav">
		<div id="layoutSidenav_nav">
			<nav class="sb-sidenav accordion sb-sidenav-dark"
				id="sidenavAccordion">
				<div class="sb-sidenav-menu">
					<div class="nav">

						<div class="sb-sidenav-menu-heading">Operador</div>

						<a class="nav-link" href="/super/view/produto">
							<div class="sb-nav-link-icon">
								<i class="fas fa-gifts"></i>
							</div> Produtos

						</a> <a class="nav-link" href="/super/view/fornecedor">
							<div class="sb-nav-link-icon">
								<i class="fas fa-user-tag"></i>
							</div> Fornecedores

						</a>

					</div>
				</div>

			</nav>
		</div>

		<div id="layoutSidenav_content">

			<form id="formFornecedor" method="get">
				<main
					style="margin-left: 10px; margin-right: 10px; margin-top: 30px; margin-bottom: 30px;">
					<div class="container-fluid">
						<div class="form-row">
							<div class="col-md-2 mb-3">
								<label>Código</label> <input type="text" class="form-control"
									name="codFornecedor" readonly
									value="${fornecedorParam.getId()}">
							</div>
							<div class="col-md-10 mb-3">
								<label>Razão Social</label> <input type="text"
									class="form-control" name="razaoSocial"
									value="${fornecedorParam.getRazaoSocial()}">
							</div>
						</div>
						<div class="form-row">
							<div class="col-md-3 mb-3">
								<label>CNPJ</label> <input type="text" class="form-control"
									name="cnpj" value="${fornecedorParam.getCnpj()}">
							</div>
							<div class="col-md-9 mb-3">
								<label>Nome Fantasia</label> <input type="text"
									class="form-control" name="nomeFantasia"
									value="${fornecedorParam.getNomeFantasia()}">
							</div>
						</div>
						<div class="form-row">
							<div class="col-md-2 mb-3">
								<label>CEP</label> <input type="text" class="form-control"
									name="cep" value="${fornecedorParam.getCep()}">
							</div>
							<div class="col-md-8 mb-3">
								<label>Logradouro</label> <input type="text"
									class="form-control" name="logradouro"
									value="${fornecedorParam.getLogradouro()}">
							</div>
							<div class="col-md-2 mb-3">
								<label>Número</label> <input type="number" step="any"
									class="form-control" name="numero"
									value="${fornecedorParam.getNumero()}">
							</div>
						</div>
						<div class="form-row">
							<div class="col-md-4 mb-3">
								<label>Bairro</label> <input type="text" class="form-control"
									name="bairro" value="${fornecedorParam.getBairro()}">
							</div>
							<div class="col-md-7 mb-3">
								<label>Cidade</label> <input type="text" class="form-control"
									name="cidade" value="${fornecedorParam.getCidade()}">
							</div>
							<div class="col-md-1 mb-3">
								<label>UF</label> <input type="text" class="form-control"
									name="uf" value="${fornecedorParam.getUf()}">
							</div>
						</div>
						<div class="form-row">
							<div class="col-md-4 mb-3">
								<label>Telefone</label> <input type="text" class="form-control"
									name="telefone" maxlength="12" autocomplete="off"
									value="${fornecedorParam.getTelefone()}">
							</div>
							<div class="col-md-8 mb-3">
								<label>E-mail</label>
								<div class="input-group mb-3">
									<input type="email" class="form-control" name="email"
										value="${fornecedorParam.getEmail()}">
									<div class="input-group-append">
										<span class="input-group-text" id="basic-addon2">@example.com</span>
									</div>
								</div>
							</div>
						</div>
						<button class="btn btn-primary" name="action" value="save"
							type="submit">Salvar</button>
					</div>
				</main>
			</form>
			
			<div class="card mb-4" style="margin-left: 20px; margin-right: 20px;">
				<div class="card-header">
					<i class="fas fa-table mr-1"></i>Fornecedores
				</div>

				<div class="card-body">
					<div>
						<table class="table table-bordered" width="100%" cellspacing="0">
							<thead>
								<th>Código</th>
								<th>CNPJ</th>
								<th>Razãzo Social</th>
								<th>Bairro</th>
								<th>Cidade</th>
								<th>Editar</th>
								<th>Excluir</th>
							</thead>
							<tbody>
								<%
									FornecedorDao dao = new FornecedorDao();
								List<Fornecedor> lista = dao.getAll();

								for (Fornecedor f : lista) {
								%>

								<tr>
									<th id="id" scope="row"><%=f.getId()%></th>
									<td><%=f.getCnpj()%></td>
									<td><%=f.getRazaoSocial()%></td>
									<td><%=f.getBairro()%></td>
									<td><%=f.getCidade()%></td>

									<td align="center"><a 
										onclick="this.href='/super/view/fornecedor?action=alt&codFornecedor='+<%=f.getId()%>">
										<i class="fas fa-user fa-edit"></i></a>
									</td>

									<td align="center"><a  name="excluirForn"
										onclick="this.href='/super/view/fornecedor?action=del&codFornecedor='+<%=f.getId()%>">
										<i class="fas fa-user fa-trash-alt"></i></a>
									</td>

								</tr>
								<%
									}
								%>
							</tbody>
						</table>
					</div>
				</div>
			</div>

		</div>

	</div>
	<script src="https://code.jquery.com/jquery-3.5.1.min.js"
		crossorigin="anonymous"></script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.bundle.min.js"
		crossorigin="anonymous"></script>
	<script src="../js/scripts.js"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js"
		crossorigin="anonymous"></script>
	<script src="../js/chart-area-demo.js"></script>
	<script src="../js/chart-bar-demo.js"></script>
	<script
		src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"
		crossorigin="anonymous"></script>
	<script
		src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"
		crossorigin="anonymous"></script>
	<script src="../js/datatables-demo.js"></script>
</body>
</html>