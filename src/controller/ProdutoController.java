package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.ProdutoDao;
import model.*;
import util.function;

 
@WebServlet( urlPatterns = {"/view/produto"})
public class ProdutoController extends HttpServlet{

	private static final long serialVresionId = 1L;
	private ProdutoDao dao = new ProdutoDao();
        
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		HttpSession session = req.getSession();
    	session.removeAttribute("produtoParam");
    	
    	String acao = req.getParameter("action");
		String id = req.getParameter("codProduto");
		id = id == null ? "" : (id.equals("0") ? "" : id);
		
			
		if(acao != null) {
			switch(acao) {
				case "save":
					Produto f = obterProduto(req);
					if(id != null && !id.isEmpty()) { //Update
						f.setId(Integer.parseInt(req.getParameter("codProduto")));
						dao.update(f);
						
					}else { //Insert
						dao.insert(f);
					}
					break;
					
				case "del":
					if(id != null && !id.isEmpty())
						dao.delete(Integer.valueOf(id));
					break;
					
				case "alt":
					if(id != null && !id.isEmpty()) {
						Produto prod = dao.getProd(Integer.valueOf(id));
						prod.setPrecoVenda(calcPrecoVenda(String.valueOf(prod.getPrecoCusto()), String.valueOf(prod.getPercLucro())));
						session.setAttribute("produtoParam", prod);
					}
					break;
			}
		}
		
		req.getRequestDispatcher("produto.jsp").forward(req,resp);
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
	}
	
	
	public Produto obterProduto(HttpServletRequest req) {
    	Produto f = new Produto();

		f.setCodBarras(function.obterSeVazio(req.getParameter("codBarras")));
		f.setNome(function.obterSeVazio(req.getParameter("nomeProduto")));
		f.setFornecedor(function.obterSeVazio(req.getParameter("fornecedorProduto")));
		f.setOrigemProduto(function.obterSeVazio(req.getParameter("origemProduto")));
		
		if(!function.obterSeVazio(req.getParameter("precoCusto")).isEmpty())
			f.setPrecoCusto(Float.parseFloat(function.obterSeVazio(req.getParameter("precoCusto"))));
		
		if(!function.obterSeVazio(req.getParameter("percLucro")).isEmpty())
			f.setPercLucro(Float.parseFloat(function.obterSeVazio(req.getParameter("percLucro"))));

    	return f;
    }
	

	
//	public void montarCadastroHtml(PrintWriter pagina, String codigo, String nome, 
//			String codBarras, String origemProduto, String precoCusto, String percLucro, String fornecedor){
//                
//                String forn = fornecedor==null?"":fornecedor;
//                nome = "value = \""+nome+"\"";
//              
//                String opcoesOrigemProduto="";
//               
//                if(origemProduto != null && !origemProduto.isEmpty()){
//                    opcoesOrigemProduto= "<option selected >"+origemProduto+"</option>\n";
//                    if(origemProduto.equals("Nacional")){
//                        opcoesOrigemProduto = opcoesOrigemProduto+" <option>Importado</option>"; 
//                    }else{
//                        opcoesOrigemProduto = opcoesOrigemProduto+" <option>Nacional</option>"; 
//                    }
//                }else{
//                    opcoesOrigemProduto = "  <option selected disabled>Selecione uma origem...</option> <option>Importado</option> <option>Nacional</option>"; 
//                }
//                
//                
//                if(fornecedor != null && !fornecedor.isEmpty()){
//                    fornecedor= "<option selected >"+fornecedor+"</option>";
//                }else{
//                    fornecedor="<option selected disabled value=\"\">Selecione um fornecedor...</option>";
//                }
//                
//                if(listaFornecedores != null){
//                    for(Fornecedor f : listaFornecedores){
//                        if(f.getRazaoSocial() != null && !f.getRazaoSocial().equals(forn))
//                            fornecedor=fornecedor+" <option>"+f.getRazaoSocial()+"</option>";
//                    }
//                }


	
	public String calcPrecoVenda(String precoCusto, String percLucro) {
		if(!precoCusto.isEmpty() && !percLucro.isEmpty()) {
			return String.valueOf(((Float.valueOf(precoCusto)/100) * Float.valueOf(percLucro)) +Float.valueOf(precoCusto));
		}
		return "";
	}
	
}
