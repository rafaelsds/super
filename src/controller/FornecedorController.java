package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.Dao;
import dao.FornecedorDao;
import model.Fornecedor;
import util.function;

 
@WebServlet( urlPatterns = {"/view/fornecedor"})
public class FornecedorController extends HttpServlet{

    private static final long serialVresionId = 1L;
    private FornecedorDao dao = new FornecedorDao();
    
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    	HttpSession session = req.getSession();
    	session.removeAttribute("fornecedorParam");
    	
    	String acao = req.getParameter("action");
		String id = req.getParameter("codFornecedor");
		id = id == null ? "" : (id.equals("0") ? "" : id);
		    	
		if(acao != null) {
			
			switch(acao) {
				case "save":
					Fornecedor f = obterFornecedor(req);
					
					if(id != null && !id.isEmpty()) { //Update
						f.setId(Integer.parseInt(id));
						dao.update(f);
						
					}else { //Insert
						dao.insert(f);
					}
					break;
					
				case "del":
					if(id != null && !id.isEmpty())
						dao.delete(Integer.valueOf(id));
					break;
					
				case "alt":
					if(id != null && !id.isEmpty()) {
						Fornecedor forn = dao.getForn(Integer.valueOf(id));
						session.setAttribute("fornecedorParam", forn);
					}
					break;
			}
		}
		
		req.getRequestDispatcher("fornecedor.jsp").forward(req,resp);
		
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    	doPost(req, resp);
    }
	
    
    public Fornecedor obterFornecedor(HttpServletRequest req) {
    	Fornecedor f = new Fornecedor();

		f.setRazaoSocial(function.obterSeVazio(req.getParameter("razaoSocial")));
		f.setNomeFantasia(function.obterSeVazio(req.getParameter("nomeFantasia")));
		f.setCnpj(function.obterSeVazio(req.getParameter("cnpj")));
		f.setCep(function.obterSeVazio(req.getParameter("cep")));
		f.setLogradouro(function.obterSeVazio(req.getParameter("logradouro")));
		f.setNumero(function.obterSeVazio(req.getParameter("numero")));
		f.setCidade(function.obterSeVazio(req.getParameter("cidade")));
		f.setUf(function.obterSeVazio(req.getParameter("uf")));
		f.setTelefone(function.obterSeVazio(req.getParameter("telefone")));
		f.setEmail(function.obterSeVazio(req.getParameter("email")));
		f.setBairro(function.obterSeVazio(req.getParameter("bairro")));
    	
    	
//		f.setRazaoSocial(obterSeValor(req.getParameter("razaoSocial")));
//		f.setNomeFantasia(obterSeValor(req.getParameter("nomeFantasia")));
//		f.setCnpj(obterSeValor(req.getParameter("cnpj")));
//		f.setCep(obterSeValor(req.getParameter("cep")));
//		f.setLogradouro(obterSeValor(req.getParameter("logradouro")));
//		f.setNumero(obterSeValor(req.getParameter("numero")));
//		f.setCidade(obterSeValor(req.getParameter("cidade")));
//		f.setUf(obterSeValor(req.getParameter("uf")));
//		f.setTelefone(obterSeValor(req.getParameter("telefone")));
//		f.setEmail(obterSeValor(req.getParameter("email")));
//		f.setBairro(obterSeValor(req.getParameter("bairro")));
    	
    	return f;
    }
    
    public String obterSeValor(String valor) {
    	return (valor != null && !valor.isEmpty()) ? valor : "";
    }
    

}
