package model;


public class Produto{
	private int id;
	private String codBarras;
	private String nome;
	private String fornecedor;
	private float precoCusto;
	private float percLucro;
	private String origemProduto;
	private String precoVenda;
	
	public Produto() {
	}
	
	
	
	public String getPrecoVenda() {
		return precoVenda;
	}

	public void setPrecoVenda(String precoVenda) {
		this.precoVenda = precoVenda;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCodBarras() {
		return codBarras;
	}
	public void setCodBarras(String codBarras) {
		this.codBarras = codBarras;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}

        public String getFornecedor() {
            return fornecedor;
        }

        public void setFornecedor(String fornecedor) {
            this.fornecedor = fornecedor;
        }


	public float getPrecoCusto() {
		return precoCusto;
	}
	public void setPrecoCusto(float precoCusto) {
		if(precoCusto>0) {
			this.precoCusto = precoCusto;
		}
	}
	public float getPercLucro() {
		return percLucro;
	}
	public void setPercLucro(float percLucro) {
		if(percLucro>0) {
			this.percLucro = percLucro;
		}
	}
	public String getOrigemProduto() {
		return origemProduto;
	}
	public void setOrigemProduto(String origemProduto) {
		this.origemProduto = origemProduto;
	}
	
	
	

}