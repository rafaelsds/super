package model;


public class Fornecedor{
    
    private int id;
    private String razaoSocial;
    private String nomeFantasia;
    private String cnpj;
    private String cep;
    private String logradouro;
    private String numero;
    private String cidade;
    private String uf;
    private String telefone;
    private String email;
    private String bairro;

    public Fornecedor() {
    }

    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRazaoSocial() {
        return razaoSocial;
    }

    public void setRazaoSocial(String razaoSocial) {
        this.razaoSocial = razaoSocial;
    }

    

    public String getNomeFantasia() {
        return nomeFantasia;
    }

    public void setNomeFantasia(String nomeFantasia) {
        this.nomeFantasia = nomeFantasia;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    

    public String getLogradouro() {
		return logradouro;
	}


	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}


	public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getUf() {
        return uf;
    }

    public void setUf(String uf) {
        this.uf = uf;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }


	public Fornecedor(int id, String razaoSocial, String nomeFantasia, String cnpj, String cep, String logradouro,
			String numero, String cidade, String uf, String telefone, String email, String bairro) {
		
		super();
		
//		if(id > 0) {
//			this.id = id;
//		}
//		
//		if(razaoSocial != null) {
//			this.razaoSocial = razaoSocial;
//		}
//		
//		if(nomeFantasia != null) {
//			this.nomeFantasia = nomeFantasia;
//		}
//		
//		if(cnpj != null) {
//			this.cnpj = cnpj;
//		}
//		
//		if(cep != null) {
//			this.cep = cep;
//		}
//		
//		if(logradouro != null) {
//			this.logradouro = logradouro;
//		}
//		
//		if(numero != null) {
//			this.numero = numero;
//		}
//		
//		if(cidade != null) {
//			this.cidade = cidade;
//		}
//		
//		if(uf != null) {
//			this.cidade = uf;
//		}
//		
//		if(telefone != null) {
//			this.telefone = telefone;
//		}
//		
//		if(email != null) {
//			this.email = email;
//		}
//		
//		if(bairro != null) {
//			this.bairro = bairro;
//		}
		
	}

    
    
    

}