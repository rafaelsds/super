package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

//import model.Fornecedor;
import model.Produto;

public class ProdutoDao extends Dao {

	/*
	  
	 	CREATE TABLE PRODUTO(ID INT PRIMARY KEY, CODBARRAS VARCHAR(255), NOME VARCHAR(255), 
		FORNECEDOR VARCHAR(255), PRECOCUSTO NUMERIC(10,2),
		PERCLUCRO NUMERIC(10,2), ORIGEMPRODUTO VARCHAR(255))
	 
	 	CREATE SEQUENCE pruduto_seq START 1;
	  
	 */

	public void update(Produto f) {
		try {
			Connection conexao = getConexao();

			PreparedStatement pstm = conexao
					.prepareStatement("Update produto SET codBarras = ?, nome = ?,	fornecedor = ?,"
							+ "	precoCusto = ?,percLucro = ?,	origemProduto = ? where id = ?");

			pstm.setString(1, f.getCodBarras());
			pstm.setString(2, f.getNome());
			pstm.setString(3, f.getFornecedor());
			pstm.setFloat(4, f.getPrecoCusto());
			pstm.setFloat(5, f.getPercLucro());
			pstm.setString(6, f.getOrigemProduto());
			pstm.setInt(7, f.getId());
			pstm.execute();
			pstm.close();
			conexao.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public boolean existe(Produto d) {
		boolean achou = false;
		try {
			Connection conexao = getConexao();
			PreparedStatement pstm = conexao.prepareStatement("Select * from produto where id = ? ");
			pstm.setInt(1, d.getId());
			ResultSet rs = pstm.executeQuery();
			if (rs.next()) {
				achou = true;
			}
			pstm.close();
			conexao.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return achou;
	}

	public int insert(Produto d) {
		
		try {
			Connection conexao = getConexao();
			PreparedStatement pstm = conexao.prepareStatement(
					"Insert into produto(id, codBarras, nome,fornecedor,precoCusto,percLucro,origemProduto) values (?,?,?,?,?,?,?)");
			pstm.setInt(1, obterSeq());
			pstm.setString(2, d.getCodBarras());
			pstm.setString(3, d.getNome());
			pstm.setString(4, d.getFornecedor());
			pstm.setFloat(5, d.getPrecoCusto());
			pstm.setFloat(6, d.getPercLucro());
			pstm.setString(7, d.getOrigemProduto());

			pstm.execute();
			pstm.close();
			conexao.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return 1;

	}

	public void delete(int id) {

		try {
			Connection conexao = getConexao();
			PreparedStatement pstm = conexao.prepareStatement("delete from produto where id = ? ");
			pstm.setInt(1, id);
			pstm.execute();
			pstm.close();
			conexao.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public List<Produto> getAll() {

		List<Produto> lista = new ArrayList<>();

		try {
			Connection conexao = getConexao();
			PreparedStatement pstm = conexao.prepareStatement("Select * from produto order by id");

			ResultSet rs = pstm.executeQuery();

			while (rs.next()) {
				Produto d = new Produto();
				d.setId(rs.getInt("id"));
				d.setCodBarras(rs.getString("codBarras"));
				d.setNome(rs.getString("nome"));
				d.setPrecoCusto(Float.parseFloat(rs.getString("precoCusto")));
				d.setPercLucro(Float.parseFloat(rs.getString("percLucro")));
				d.setOrigemProduto(rs.getString("origemProduto"));
				d.setFornecedor(rs.getString("fornecedor"));
				lista.add(d);
			}

			pstm.close();
			conexao.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lista;
	}

	public Produto getProd(int id) {
		Produto d = new Produto();
		try {
			Connection conexao = getConexao();
			PreparedStatement pstm = conexao.prepareStatement("Select * from produto where id = ?");

			pstm.setInt(1, id);
			ResultSet rs = pstm.executeQuery();

			while (rs.next()) {
				d.setId(rs.getInt("id"));
				d.setCodBarras(rs.getString("codBarras"));
				d.setNome(rs.getString("nome"));
				d.setPrecoCusto(Float.parseFloat(rs.getString("precoCusto")));
				d.setPercLucro(Float.parseFloat(rs.getString("percLucro")));
				d.setOrigemProduto(rs.getString("origemProduto"));
				d.setFornecedor(rs.getString("fornecedor"));
			}

			pstm.close();
			conexao.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return d;
	}

	public int obterSeq() {
		int seq = 0;

		try {
			Connection conexao = getConexao();
			PreparedStatement pstm = conexao.prepareStatement("SELECT nextval('produto_seq')seq");

			ResultSet rs = pstm.executeQuery();
			if (rs.next()) {
				seq = rs.getInt("seq");
			}
			pstm.close();
			conexao.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return seq;
	}

}