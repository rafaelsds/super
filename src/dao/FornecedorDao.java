package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.Fornecedor;

public class FornecedorDao extends Dao {

	/*
	 * 
	 * CREATE TABLE fornecedor(id INT PRIMARY key, razaoSocial VARCHAR(255),
	 * nomeFantasia VARCHAR(255), cnpj VARCHAR(25), cep VARCHAR(10), numero
	 * VARCHAR(10), cidade VARCHAR(255), uf VARCHAR(2), telefone VARCHAR(15), email
	 * VARCHAR(255), bairro VARCHAR(255), logradouro VARCHAR(255))
	 * 
	 * CREATE SEQUENCE fornecedor_seq START 1;
	 * 
	 */

	public void update(Fornecedor f) {
		try {
			Connection conexao = getConexao();

			PreparedStatement pstm = conexao
					.prepareStatement("Update fornecedor SET razaoSocial = ?, nomeFantasia = ?,	cnpj = ?,"
							+ "	cep = ?,numero = ?,	cidade = ?,  uf = ?, 	telefone = ?, "
							+ " email = ?,  bairro = ?, logradouro = ? where id = ?");

			pstm.setString(1, f.getRazaoSocial());
			pstm.setString(2, f.getNomeFantasia());
			pstm.setString(3, f.getCnpj());
			pstm.setString(4, f.getCep());
			pstm.setString(5, f.getNumero());
			pstm.setString(6, f.getCidade());
			pstm.setString(7, f.getUf());
			pstm.setString(8, f.getTelefone());
			pstm.setString(9, f.getEmail());
			pstm.setString(10, f.getBairro());
			pstm.setString(11, f.getLogradouro());
			pstm.setInt(12, f.getId());
			pstm.execute();
			pstm.close();
			conexao.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public boolean existe(Fornecedor d) {
		boolean achou = false;
		try {
			Connection conexao = getConexao();
			PreparedStatement pstm = conexao.prepareStatement("Select * from fornecedor where id = ? ");
			pstm.setInt(1, d.getId());
			ResultSet rs = pstm.executeQuery();
			if (rs.next()) {
				achou = true;
			}
			pstm.close();
			conexao.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return achou;
	}

	public int insert(Fornecedor d) {
		
		try {
			Connection conexao = getConexao();
			PreparedStatement pstm = conexao.prepareStatement(
					"Insert into	fornecedor (id, razaoSocial, nomeFantasia,cnpj,cep,numero,cidade, uf,telefone, email, bairro,logradouro ) values (?,?,?,?,?,?,?,?,?,?,?,?)");
			pstm.setInt(1, obterSeq());
			pstm.setString(2, d.getRazaoSocial());
			pstm.setString(3, d.getNomeFantasia());
			pstm.setString(4, d.getCnpj());
			pstm.setString(5, d.getCep());
			pstm.setString(6, d.getNumero());
			pstm.setString(7, d.getCidade());
			pstm.setString(8, d.getUf());
			pstm.setString(9, d.getTelefone());
			pstm.setString(10, d.getEmail());
			pstm.setString(11, d.getBairro());
			pstm.setString(12, d.getLogradouro());
			pstm.execute();
			pstm.close();
			conexao.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return 1;

	}

	public void delete(int id) {

		try {
			Connection conexao = getConexao();
			PreparedStatement pstm = conexao.prepareStatement("delete from fornecedor where id = ? ");
			pstm.setInt(1, id);
			pstm.execute();
			pstm.close();
			conexao.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public List<Fornecedor> getAll() {

		List<Fornecedor> lista = new ArrayList<>();

		try {
			Connection conexao = getConexao();
			PreparedStatement pstm = conexao.prepareStatement("Select * from fornecedor order by id");

			ResultSet rs = pstm.executeQuery();

			while (rs.next()) {
				Fornecedor d = new Fornecedor();
				d.setId(rs.getInt("id"));
				d.setRazaoSocial(rs.getString("razaoSocial"));
				d.setNomeFantasia(rs.getString("nomeFantasia"));
				d.setCnpj(rs.getString("cnpj"));
				d.setCep(rs.getString("cep"));
				d.setNumero(rs.getString("numero"));
				d.setCidade(rs.getString("cidade"));
				d.setUf(rs.getString("uf"));
				d.setTelefone(rs.getString("telefone"));
				d.setLogradouro(rs.getString("logradouro"));
				d.setEmail(rs.getString("email"));
				d.setBairro(rs.getString("bairro"));
				lista.add(d);
			}

			pstm.close();
			conexao.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lista;
	}

	public List<Fornecedor> getAllDif(String forn) {

		List<Fornecedor> lista = new ArrayList<>();

		try {
			Connection conexao = getConexao();
			PreparedStatement pstm = conexao.prepareStatement("Select * from fornecedor where razaoSocial != ? order by id");
			
			pstm.setString(1, forn);
			ResultSet rs = pstm.executeQuery();

			while (rs.next()) {
				Fornecedor d = new Fornecedor();
				d.setId(rs.getInt("id"));
				d.setRazaoSocial(rs.getString("razaoSocial"));
				d.setNomeFantasia(rs.getString("nomeFantasia"));
				d.setCnpj(rs.getString("cnpj"));
				d.setCep(rs.getString("cep"));
				d.setNumero(rs.getString("numero"));
				d.setCidade(rs.getString("cidade"));
				d.setUf(rs.getString("uf"));
				d.setTelefone(rs.getString("telefone"));
				d.setLogradouro(rs.getString("logradouro"));
				d.setEmail(rs.getString("email"));
				d.setBairro(rs.getString("bairro"));
				lista.add(d);
			}

			pstm.close();
			conexao.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lista;
	}
	
	public Fornecedor getForn(int id) {
		Fornecedor d = new Fornecedor();
		try {
			Connection conexao = getConexao();
			PreparedStatement pstm = conexao.prepareStatement("Select * from fornecedor where id = ?");

			pstm.setInt(1, id);
			ResultSet rs = pstm.executeQuery();

			while (rs.next()) {

				d.setId(rs.getInt("id"));
				d.setRazaoSocial(rs.getString("razaoSocial"));
				d.setNomeFantasia(rs.getString("nomeFantasia"));
				d.setCnpj(rs.getString("cnpj"));
				d.setCep(rs.getString("cep"));
				d.setNumero(rs.getString("numero"));
				d.setCidade(rs.getString("cidade"));
				d.setUf(rs.getString("uf"));
				d.setTelefone(rs.getString("telefone"));
				d.setLogradouro(rs.getString("logradouro"));
				d.setEmail(rs.getString("email"));
				d.setBairro(rs.getString("bairro"));
			}

			pstm.close();
			conexao.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return d;
	}

	public int obterSeq() {
		int seq = 0;

		try {
			Connection conexao = getConexao();
			PreparedStatement pstm = conexao.prepareStatement("SELECT nextval('fornecedor_seq')seq");

			ResultSet rs = pstm.executeQuery();
			if (rs.next()) {
				seq = rs.getInt("seq");
			}
			pstm.close();
			conexao.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return seq;
	}

}